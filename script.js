function Helper(){
	this.inputs = [];
	this.flag = true;
	
	this.showInput = function(){
		for(i = 0; i < this.inputs.length; i++){
			console.log(this.inputs[i]);
		};
	};
	
	this.getLength = function(){
		return this.inputs.length;
	}
	
	this.setInput = function(maxCount){
		if(this.inputs.length > maxCount){
			this.flag = false;
		}
		
		var input = document.createElement('input');
		this.inputs.push(input);
		$(input).addClass('main_text');
		$(".input").append(input);
		$(input).focus();
	};
	
	this.getFlag = function(){
		return this.flag;
	}
	
	this.deleteInput = function(){
		var currentInput = this.inputs.pop();
		$(currentInput).remove();
		$(this.inputs[this.inputs.length-1]).prop('disabled',false);
		$(this.inputs[this.inputs.length-1]).val("");
		$(this.inputs[this.inputs.length-1]).focus();
		
		if(this.inputs.length === 1){
			this.flag = true;
		}
	};
};


var userFunc = function(e,helper){
	var message = $(e.currentTarget).val();
	
	if(message.length > 11){
		if(helper.getFlag()){	
			$(e.currentTarget).prop('disabled',true);
			helper.setInput(10);
		}
		else{
			helper.deleteInput();
		};
	};		
};

function Changer(){
	var helper = new Helper();
	helper.setInput();
	
	this.change = function(eventName,className,func){
		$("body").on(eventName,className,function(e){
			func(e,helper);
		});
	};
};

$(document).ready(function(){
	var changer = new Changer();
	changer.change('keydown','.main_text',userFunc);
});